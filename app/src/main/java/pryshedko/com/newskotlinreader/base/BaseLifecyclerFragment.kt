package pryshedko.com.newskotlinreader.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.databinding.FragmentFavoritesBinding
import pryshedko.com.newskotlinreader.unsafeLazy

abstract class BaseLifecyclerFragment<T : ViewModel> : Fragment() {
    abstract val viewModelClass: Class<T>
    protected val viewModel: T by unsafeLazy { ViewModelProviders.of(this).get(viewModelClass) }
}