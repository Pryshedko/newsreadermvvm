package pryshedko.com.newskotlinreader.base

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import pryshedko.com.newskotlinreader.unsafeLazy

abstract class BaseLifecyclerActivity<T : ViewModel> : AppCompatActivity(){

    abstract val viewModelClass: Class<T>

    protected val viewModel: T by unsafeLazy { ViewModelProviders.of(this).get(viewModelClass) }

    private val registry = LifecycleRegistry(this)

    override fun getLifecycle(): LifecycleRegistry = registry

}