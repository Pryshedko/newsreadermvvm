package pryshedko.com.newskotlinreader.base

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Anton on 17.03.2018.
 */
abstract class BaseViewHolder<D>(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    abstract fun onBind(item: D)
}