package pryshedko.com.newskotlinreader.repository

import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties

interface ArticlesRepository {
    fun postsOfArticles(properties:SearchProperties): Listing<Article>
}