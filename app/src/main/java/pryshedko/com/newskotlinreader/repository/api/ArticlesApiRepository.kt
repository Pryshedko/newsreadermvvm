package pryshedko.com.newskotlinreader.repository.api

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.support.annotation.MainThread
import com.example.user.paginglibrarytest.ArticlesDataSourceFactory
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.repository.ArticlesRepository
import pryshedko.com.newskotlinreader.repository.Listing
import java.util.concurrent.Executors

class ArticlesApiRepository : ArticlesRepository {

    @MainThread
    override fun postsOfArticles(properties: SearchProperties): Listing<Article> {
        val factory = ArticlesDataSourceFactory(properties)

        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(properties.pageSize)
                .build()

        var pagedList: LiveData<PagedList<Article>> = LivePagedListBuilder(factory, config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build()

        return Listing(pagedList,
                Transformations.switchMap(factory.sourceLiveData, { it.networkState }),
                retry = { factory.sourceLiveData.value?.retryOnError() },
                refresh = { factory.sourceLiveData.value?.invalidate() }
        )
    }
}