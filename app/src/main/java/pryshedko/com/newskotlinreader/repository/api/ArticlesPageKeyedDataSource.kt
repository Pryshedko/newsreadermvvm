package com.example.user.paginglibrarytest

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.repository.NetworkState
import pryshedko.com.newskotlinreader.source.api.RemoteDataSource
import kotlin.concurrent.thread

class ArticlesPageKeyedDataSource(var searchProperties: SearchProperties) : PageKeyedDataSource<Int, Article>() {

    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()

    val compositeDisposable = CompositeDisposable()

    val TAG: String = "PageKeyed"

    fun retryOnError() {
        val prevRetry = retry
        retry = null
        prevRetry?.let { thread { it.invoke() } }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        Log.i(TAG, "load after: ${params.key} ${params.requestedLoadSize}")

        searchProperties?.let {
            compositeDisposable.add(RemoteDataSource.getArticles(searchProperties, params.key)
                    .doOnSubscribe {
                        networkState.postValue(NetworkState.LOADING)
                    }.subscribe({ it ->
                        networkState.postValue(NetworkState.LOADED)
                        callback.onResult(it, params.key + 1)
                    }, {
                        networkState.postValue(NetworkState.error(it.message))
                        retry = { loadAfter(params, callback) }
                    }))
        }

    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Article>) {
        Log.i(TAG, "load initial: " + params.requestedLoadSize)

        searchProperties.let {
            compositeDisposable.add(RemoteDataSource.getArticles(searchProperties, 1)
                    .doOnSubscribe {
                        networkState.postValue(NetworkState.LOADING)
                    }.subscribe({ it ->
                        networkState.postValue(NetworkState.LOADED)
                        callback.onResult(it, 1, 2)
                    }, {
                        networkState.postValue(NetworkState.error(it.message))
                        retry = { loadInitial(params, callback) }
                    }))
        }

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {}

}
