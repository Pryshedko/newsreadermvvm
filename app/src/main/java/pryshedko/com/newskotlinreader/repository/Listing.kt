package pryshedko.com.newskotlinreader.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class Listing<T>(
        val pagedList: LiveData<PagedList<T>>,
        val networkState: LiveData<NetworkState>,
        val retry: () -> Unit,
        val refresh: () -> Unit
)
