package com.example.user.paginglibrarytest

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties

class ArticlesDataSourceFactory(var properties: SearchProperties) : DataSource.Factory<Int, Article>() {
    val sourceLiveData = MutableLiveData<ArticlesPageKeyedDataSource>()
    override fun create(): DataSource<Int, Article>? {
        val source = ArticlesPageKeyedDataSource(properties)
        sourceLiveData.postValue(source)
        return source
    }
}