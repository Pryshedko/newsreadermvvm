package pryshedko.com.newskotlinreader.source.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.Source
import pryshedko.com.newskotlinreader.source.db.dao.ArticlesDao
import pryshedko.com.newskotlinreader.source.db.dao.SourcesDao

@Database(entities = arrayOf(Article::class, Source::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun articlesDao():ArticlesDao
    abstract fun sourcesDao(): SourcesDao

    companion object {
        const val DATABASE_NAME = "basic-sample-db"
    }
}
