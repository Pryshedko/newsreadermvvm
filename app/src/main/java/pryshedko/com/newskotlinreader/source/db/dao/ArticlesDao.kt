package pryshedko.com.newskotlinreader.source.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import io.reactivex.Flowable
import io.reactivex.Single
import pryshedko.com.newskotlinreader.entities.Article

@Dao
interface ArticlesDao {

    @Query("SELECT * FROM articles")
    fun loadAllArticles(): Flowable<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(article: Article)

    @Delete
    fun delete(article: Article)

}