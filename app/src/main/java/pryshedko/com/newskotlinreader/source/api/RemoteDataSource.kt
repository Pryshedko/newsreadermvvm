package pryshedko.com.newskotlinreader.source.api

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.interceptors.loggingResponseInterceptor
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.rx.rx_object
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.BaseResponse
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.entities.Source
import pryshedko.com.newskotlinreader.source.DataSource

object RemoteDataSource : DataSource {

    init {
        FuelManager.instance.basePath = "https://newsapi.org/"
        FuelManager.instance.baseParams = listOf("apiKey" to "033d6adc04ad4c67aab48e5fdb9466fe")
        FuelManager.instance.addResponseInterceptor { loggingResponseInterceptor() }
    }

    override fun getAllArticles(): Flowable<List<Article>>? = null

    override fun getArticles(properties: SearchProperties, pageKey: Int): Single<List<Article>> =
            "/v2/everything"
                    .httpGet(listOf("sortBy" to properties.sortBy,
                            "sources" to properties.getSelectedSources(),
                            "from" to properties.getDateFromStr(),
                            "to" to properties.getDateToStr(),
                            "page" to pageKey,
                            "pageSize" to 40))
                    .rx_object(BaseResponse.Deserializer())
                    .map { it.component1()?.articles ?: throw it?.component2() ?: throw Exception() }


    override fun getSources(): Single<List<Source>> =
            "/v2/sources".httpGet(listOf("language" to "en"))
                    .rx_object(BaseResponse.Deserializer())
                    .map { it.component1()?.sources ?: throw it?.component2() ?: throw Exception() }

}