package pryshedko.com.newskotlinreader.source

import io.reactivex.Flowable
import io.reactivex.Single
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.entities.Source

interface DataSource {
    fun getArticles(properties: SearchProperties, pageKey: Int): Single<List<Article>>
    fun getSources(): Single<List<Source>>

    fun saveSources(sources: List<Source>): Unit = Unit
    fun getAllArticles(): Flowable<List<Article>>?
    fun saveArticle(article: Article):Unit=Unit
    fun deleteArticle(article:Article):Unit=Unit
}