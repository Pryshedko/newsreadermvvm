package pryshedko.com.newskotlinreader.source.db

import io.reactivex.Flowable
import io.reactivex.Single
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.entities.Source
import pryshedko.com.newskotlinreader.source.DataSource

object LocalDataSource : DataSource {

    private val articlesDao = DatabaseCreator.database.articlesDao()
    private val sourcesDao = DatabaseCreator.database.sourcesDao()

    override fun getArticles(properties: SearchProperties, pageKey: Int): Single<List<Article>> = articlesDao.loadAllArticles()
            .firstOrError()
            .doOnSuccess { if (it.isEmpty()) throw Exception() }

    override fun getAllArticles() : Flowable<List<Article>> = articlesDao.loadAllArticles()

    override fun deleteArticle(article: Article) = articlesDao.delete(article)

    override fun saveArticle(article: Article) = articlesDao.insertArticle(article)

    override fun saveSources(sources: List<Source>) = sourcesDao.insertSources(sources)

    override fun getSources(): Single<List<Source>> = sourcesDao.loadAllSources()


}