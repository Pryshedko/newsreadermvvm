package pryshedko.com.newskotlinreader.source.db

import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.Room
import android.content.Context
import pryshedko.com.newskotlinreader.source.db.AppDatabase.Companion.DATABASE_NAME
import java.util.concurrent.atomic.AtomicBoolean

object DatabaseCreator {

    val isDatabaseCreated = MutableLiveData<Boolean>()

    lateinit var database: AppDatabase

    private val mInitializing = AtomicBoolean(true)

    fun createDb(context: Context) {

        if (mInitializing.compareAndSet(true, false).not())
            return

        isDatabaseCreated.value = false

        database = Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()

        isDatabaseCreated.value = true
    }
}