package pryshedko.com.newskotlinreader.source.db.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import io.reactivex.Single
import pryshedko.com.newskotlinreader.entities.Source

@Dao
interface SourcesDao {

    @Query("SELECT * FROM sources")
    fun loadAllSources(): Single<List<Source>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSources(sources: List<Source>)

    @Query("DELETE FROM sources")
    fun deleteAllSources()

    @Delete
    fun delete(source: Source)
}