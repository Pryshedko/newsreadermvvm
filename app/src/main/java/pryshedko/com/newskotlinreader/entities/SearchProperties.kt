package pryshedko.com.newskotlinreader.entities

import java.text.SimpleDateFormat
import java.util.*

class SearchProperties {
    var selectedItem = 0
    var sortBy = listOf<String>()
    var sources = listOf<Source>()
    var fromDate = Calendar.getInstance().time
    var toDate = Calendar.getInstance().time
    var pageSize = 40

    constructor()

    constructor(sources: List<Source>) {
        this.sources = sources
    }

    fun getSourtBy() = sortBy.get(selectedItem)

    fun getSelectedSources(): String {
        var selectedSources = mutableListOf<Source>()
        sources.forEach { if (it.isSelected) selectedSources.add(it) }
        return selectedSources.joinToString(",")
    }

    fun getDateToStr() = strFromDate(toDate)
    fun getDateFromStr() = strFromDate(fromDate)

    private fun strFromDate(date: Date): String {
        val dateFormatServer = SimpleDateFormat("yyyy-MM-dd")
        return dateFormatServer.format(date)
    }
}