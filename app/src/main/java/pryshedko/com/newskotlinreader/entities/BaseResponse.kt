package pryshedko.com.newskotlinreader.entities

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.*

class BaseResponse(
        var status: String?,
        var message: String?,
        var totalResults: Int?,
        var articles: List<Article>?,
        var sources: List<Source>?
) {
    class Deserializer : ResponseDeserializable<BaseResponse> {
        override fun deserialize(content: String) = Gson().fromJson(content, BaseResponse::class.java)
    }
}