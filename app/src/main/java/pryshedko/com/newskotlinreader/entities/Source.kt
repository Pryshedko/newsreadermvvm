package pryshedko.com.newskotlinreader.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "sources")
public data class Source(
        var id: String?,
        @PrimaryKey var name: String,
        var description: String?,
        var url: String?,
        var category: String?,
        var language: String?,
        var country: String?,
        @Ignore var isSelected: Boolean = false
){
    constructor() : this("","", "", "", "", "", "",false)
}
