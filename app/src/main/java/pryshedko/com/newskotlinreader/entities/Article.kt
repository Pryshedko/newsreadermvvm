package pryshedko.com.newskotlinreader.entities

import android.arch.persistence.room.*

@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Entity(tableName = "articles")
data class Article(
        @PrimaryKey var url: String,
        @Embedded(prefix = "source_") var source: Source?,
        var title: String?,
        var description: String?,
        var urlToImage: String?,
        var publishedAt: String?,
        @Ignore var isFavorite: Boolean = false
) {
    constructor() : this("", Source(), "", "", "", "", false)
}
