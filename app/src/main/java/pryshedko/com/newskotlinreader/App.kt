package pryshedko.com.newskotlinreader

import android.app.Application
import pryshedko.com.newskotlinreader.source.db.DatabaseCreator

class App:Application(){
    override fun onCreate() {
        super.onCreate()
        DatabaseCreator.createDb(this)
    }
}