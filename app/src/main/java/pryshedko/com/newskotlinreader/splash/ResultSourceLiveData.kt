package pryshedko.com.newskotlinreader.splash

import android.arch.lifecycle.MediatorLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pryshedko.com.newskotlinreader.entities.Source
import pryshedko.com.newskotlinreader.source.api.RemoteDataSource
import pryshedko.com.newskotlinreader.source.db.LocalDataSource

class ResultSourceLiveData(private val isLoading: (Boolean) -> Unit) : MediatorLiveData<List<Source>>() {

    private var disposable: Disposable? = null

    fun downloadSources() {
        isLoading.invoke(true)
        disposable = LocalDataSource
                .getSources()
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess { if (it.isEmpty()) throw Exception("Start download, sources is empty") }
                .onErrorResumeNext {
                    RemoteDataSource
                            .getSources()
                            .doOnSuccess { LocalDataSource.saveSources(it) }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    this.value = it
                }, {
                    isLoading.invoke(false)
                })
    }


    override fun onInactive() {
        super.onInactive()
        if (disposable?.isDisposed?.not() ?: false) {
            disposable?.dispose()
        }
    }
}