package pryshedko.com.newskotlinreader.splash

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean

class SplashViewModel : ViewModel() {

    val isLoading = ObservableBoolean(false)

    val resultLiveData = ResultSourceLiveData{isLoading.set(it)}

    fun start() = resultLiveData.downloadSources()

}