package pryshedko.com.newskotlinreader.splash

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_splash.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.base.BaseLifecyclerActivity
import pryshedko.com.newskotlinreader.main.MainActivity
import android.databinding.DataBindingUtil
import pryshedko.com.newskotlinreader.databinding.ActivitySplashBinding


class SplashActivity : BaseLifecyclerActivity<SplashViewModel>() {

    override val viewModelClass = SplashViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)
        binding.viewmodel = viewModel

        if (savedInstanceState == null)
            viewModel.start()

        btn_retry.setOnClickListener { viewModel.start() }
        btn_start_offline.setOnClickListener { startMainActivityOffline() }

        observeLiveData()
    }

    private fun observeLiveData() {
        viewModel.resultLiveData.observe(this, Observer { it?.let { startMainActivity() } })
    }

    private fun startMainActivity() = startActivity(Intent(this, MainActivity::class.java))

    //Todo set parameter for offline
    private fun startMainActivityOffline() = startActivity(Intent(this, MainActivity::class.java))
}