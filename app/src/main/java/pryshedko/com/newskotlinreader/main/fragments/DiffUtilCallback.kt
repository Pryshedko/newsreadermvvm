package pryshedko.com.newskotlinreader.main.fragments

import android.support.v7.util.DiffUtil
import pryshedko.com.newskotlinreader.entities.Article

class DiffUtilCallback : DiffUtil.ItemCallback<Article>() {

        override fun areItemsTheSame(oldItem: Article?, newItem: Article?): Boolean {
            return oldItem?.url.equals(newItem?.url)
        }

        override fun areContentsTheSame(oldItem: Article?, newItem: Article?): Boolean {
            return oldItem?.description.equals(newItem?.description) &&
                    oldItem?.title.equals(newItem?.title)
        }
    }