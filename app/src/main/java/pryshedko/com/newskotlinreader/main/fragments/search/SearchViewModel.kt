package pryshedko.com.newskotlinreader.main.fragments.search

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.Transformations.switchMap
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.main.fragments.LocalArticlesLiveData
import pryshedko.com.newskotlinreader.main.fragments.LocalSourcesLiveData
import pryshedko.com.newskotlinreader.repository.api.ArticlesApiRepository
import pryshedko.com.newskotlinreader.source.db.LocalDataSource
import kotlin.concurrent.thread

class SearchViewModel : ViewModel() {

    val isFabVisible = ObservableBoolean(false)

    val isDialogOpenLiveData = MediatorLiveData<Boolean>()

    val sourcesLiveData = LocalSourcesLiveData()

    val searchPropertiesLiveData = MediatorLiveData<SearchProperties>().apply {
        addSource(sourcesLiveData) {
            it?.let {
                isFabVisible.set(true)
                this.value = SearchProperties(it)
            }
        }
    }

    val repoResoultLiveData = map(searchPropertiesLiveData, { ArticlesApiRepository().postsOfArticles(it) })

    val remoteArticlesLiveData = switchMap(repoResoultLiveData, { it.pagedList })

    val networkStateLiveData = switchMap(repoResoultLiveData, { it.networkState })

    fun retry() = repoResoultLiveData.value?.retry?.invoke()

    val localArticlesLiveData = LocalArticlesLiveData({})

    fun start() = sourcesLiveData.downloadSources()

    fun saveArticle(article: Article) = thread { LocalDataSource.saveArticle(article) }

    fun deleteArticle(article: Article) = thread { LocalDataSource.deleteArticle(article) }
}