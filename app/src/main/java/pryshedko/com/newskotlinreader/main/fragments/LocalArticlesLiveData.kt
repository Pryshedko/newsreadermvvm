package pryshedko.com.newskotlinreader.main.fragments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.util.Log
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.source.db.LocalDataSource

class LocalArticlesLiveData(private val isLoading: (Boolean) -> Unit) : LiveData<List<Article>>() {

    private var disposable: Disposable? = null

    override fun onActive() {
        super.onActive()
        isLoading.invoke(true)
        disposable = LocalDataSource.getAllArticles()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    value = it
                    isLoading.invoke(false)
                }, {
                    error ->
                    isLoading.invoke(false)
                })
    }

    override fun onInactive() {
        super.onInactive()
        if (disposable?.isDisposed?.not() ?: false) {
            disposable?.dispose()
        }
    }
}