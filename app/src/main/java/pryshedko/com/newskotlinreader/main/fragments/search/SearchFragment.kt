package pryshedko.com.newskotlinreader.main.fragments.search

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.user.paginglibrarytest.ArticleAdapter
import kotlinx.android.synthetic.main.fragment_search.view.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.base.BaseLifecyclerFragment
import pryshedko.com.newskotlinreader.databinding.FragmentSearchBinding
import pryshedko.com.newskotlinreader.main.fragments.search.dialog.DialogSearchProperties
import pryshedko.com.newskotlinreader.repository.NetworkState
import pryshedko.com.newskotlinreader.repository.Status

class SearchFragment : BaseLifecyclerFragment<SearchViewModel>() {

    override val viewModelClass = SearchViewModel::class.java

    private lateinit var adapter: ArticleAdapter
    private lateinit var dialog: DialogSearchProperties

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding = DataBindingUtil.inflate<FragmentSearchBinding>(inflater, R.layout.fragment_search, container, false)

        binding.viewmodel = viewModel

        adapter = ArticleAdapter(Glide.with(this), { viewModel.saveArticle(it) }, { viewModel.deleteArticle(it) }, { viewModel.retry() })

        binding.root.rv.adapter = adapter
        binding.root.fab.setOnClickListener { dialog.show() }

        if (savedInstanceState == null)
            viewModel.start()

        dialog = DialogSearchProperties(context)
        dialog.setOnSearchListener { viewModel.searchPropertiesLiveData.value = it }
        dialog.setOnDismissListener { viewModel.isDialogOpenLiveData.value = false }
        dialog.setOnShowListener { viewModel.isDialogOpenLiveData.value = true }

        observeLiveData()

        return binding.root
    }


    private fun observeLiveData() {

        viewModel.remoteArticlesLiveData.observe(this, Observer {
            it?.let { adapter?.submitList(it) }
        })

        viewModel.localArticlesLiveData.observe(this, Observer {
            it?.let { adapter.updateLocalArticles(it) }
        })

        viewModel.networkStateLiveData.observe(this, Observer {
            it?.let {
                adapter.setNetworkState(it)
                if (it.status == Status.FAILED) Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.searchPropertiesLiveData.observe(this, Observer { it?.let { dialog.properties = it } })

        viewModel.repoResoultLiveData.observe(this, Observer { })

        viewModel.isDialogOpenLiveData.observe(this, Observer<Boolean> { it?.let { if (it) dialog.show() } })
    }
}
