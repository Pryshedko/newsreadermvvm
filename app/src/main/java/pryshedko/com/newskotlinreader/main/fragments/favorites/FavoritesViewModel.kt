package pryshedko.com.newskotlinreader.main.fragments.favorites

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.main.fragments.LocalArticlesLiveData
import pryshedko.com.newskotlinreader.source.db.LocalDataSource
import kotlin.concurrent.thread


class FavoritesViewModel : ViewModel() {

    val isLoadingVisible = ObservableBoolean(true)

    val localArticlesLiveData = LocalArticlesLiveData({ isLoadingVisible.set(it) })

    fun deleteArticle(article: Article) = thread { LocalDataSource.deleteArticle(article)}

}