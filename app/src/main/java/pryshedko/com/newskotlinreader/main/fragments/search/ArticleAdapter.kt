package com.example.user.paginglibrarytest

import android.arch.paging.PagedList
import android.arch.paging.PagedListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.item_recyclerview_article.view.*
import kotlinx.android.synthetic.main.item_recyclerview_network_state.view.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.main.fragments.DiffUtilCallback
import pryshedko.com.newskotlinreader.repository.NetworkState
import pryshedko.com.newskotlinreader.repository.Status

class ArticleAdapter(private val glide: RequestManager,
                     private val saveArticleCallback: (Article) -> Unit,
                     private val deleteArticleCallback: (Article) -> Unit,
                     private val retryCallback: () -> Unit) : PagedListAdapter<Article, RecyclerView.ViewHolder>(DiffUtilCallback()) {

    var localArticles = listOf<Article>()

    fun updateLocalArticles(articles: List<Article>) {
        localArticles = articles
        updateArticlesStatus()
    }

    private fun updateArticlesStatus() {
        currentList?.forEach {
            if (isArticleLocal(it))
                it.isFavorite = true
            else it.isFavorite = false
        }
        notifyDataSetChanged()
    }

    private fun isArticleLocal(article: Article): Boolean {
        localArticles.forEach { if (article.url.equals(it.url)) return true }
        return false
    }

    private var networkState: NetworkState? = null

    override fun onCurrentListChanged(currentList: PagedList<Article>?) {
        super.onCurrentListChanged(currentList)
        updateArticlesStatus()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_recyclerview_article -> (holder as RemoteArticleViewHolder).bindData(position)
            R.layout.item_recyclerview_network_state -> (holder as NetworkStateViewHolder).bindData(networkState)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        when (viewType) {
            R.layout.item_recyclerview_article -> return RemoteArticleViewHolder(view)
            R.layout.item_recyclerview_network_state -> return NetworkStateViewHolder(view)
            else -> return RemoteArticleViewHolder(view)
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemCount(): Int = super.getItemCount() + if (hasExtraRow()) 1 else 0

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1)
            R.layout.item_recyclerview_network_state
        else
            R.layout.item_recyclerview_article

    }

    fun setNetworkState(newNetworkState: NetworkState) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    inner class RemoteArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.image_view
        val title = itemView.tv_title
        val source = itemView.tv_source
        val checkBox = itemView.checkbox

        fun bindData(position: Int) {
            var article = getItem(position)

            article?.let {
                title.text = it.title
                source.text = it.source?.name
                checkBox.isChecked = it?.isFavorite
                glide.load(it?.urlToImage).into(image)

                checkBox.setOnClickListener { view ->
                    if (it?.isFavorite)
                        deleteArticleCallback.invoke(article)
                    else
                        saveArticleCallback.invoke(article)

                }
            }

        }
    }

    inner class NetworkStateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val progressBar = itemView.progress_bar
        val btn_retry = itemView.btn_retry

        fun bindData(networkState: NetworkState?) {
            btn_retry.setOnClickListener { retryCallback.invoke() }

            when (networkState?.status) {
                Status.RUNNING -> {
                    progressBar.visibility = View.VISIBLE
                    btn_retry.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    btn_retry.visibility = View.GONE
                }
                Status.FAILED -> {
                    progressBar.visibility = View.GONE
                    btn_retry.visibility = View.VISIBLE
                }
            }
        }
    }
}
