package pryshedko.com.newskotlinreader.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.main.fragments.favorites.FavoritesFragment
import pryshedko.com.newskotlinreader.main.fragments.search.SearchFragment
import pryshedko.com.newskotlinreader.unsafeLazy

class MainActivity:AppCompatActivity() {

    private val adapter by unsafeLazy { ScreenSlidePagerAdapter(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter.addFragment(SearchFragment())
        adapter.addFragment(FavoritesFragment())
        pager.adapter = adapter
    }
}
