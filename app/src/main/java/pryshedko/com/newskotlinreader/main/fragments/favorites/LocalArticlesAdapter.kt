package pryshedko.com.newskotlinreader.main.fragments.favorites

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.item_recyclerview_local_article.view.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.main.fragments.DiffUtilCallback
import pryshedko.com.newskotlinreader.main.fragments.favorites.LocalArticlesAdapter.LocalArticleViewHolder

class LocalArticlesAdapter(private val glide: RequestManager,
                           private val deleteListener: (Article) -> Unit,
                           private val openUrlListener: (Article) -> Unit) : ListAdapter<Article, LocalArticleViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocalArticleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return LocalArticleViewHolder(inflater.inflate(R.layout.item_recyclerview_local_article, parent, false))
    }

    override fun onBindViewHolder(holder: LocalArticleViewHolder, position: Int) {
        holder.bindData(getItem(position), glide)
    }

    inner class LocalArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.image_view
        val title = itemView.tv_title
        val source = itemView.tv_source

        var btnDeleteItem = itemView.btn_delete
        var btnOpenUrl = itemView.btn_open_url

        fun bindData(artice: Article, glide: RequestManager) {
            title.text = artice?.title
            source.text = artice?.source?.name
            glide.load(artice?.urlToImage).into(image)
            btnDeleteItem.setOnClickListener { deleteListener.invoke(artice) }
            btnOpenUrl.setOnClickListener { openUrlListener.invoke(artice) }
        }
    }
}