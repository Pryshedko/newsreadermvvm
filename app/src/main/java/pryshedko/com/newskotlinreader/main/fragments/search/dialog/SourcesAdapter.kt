package pryshedko.com.newskotlinreader.main.fragments.search.dialog

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import kotlinx.android.synthetic.main.item_recyclerview_sources.view.*

import java.util.ArrayList

import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.entities.Source

class SourcesAdapter : RecyclerView.Adapter<SourcesAdapter.ViewHolder>() {

    private var sources = listOf<Source>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_sources, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(sources.get(position))
    }

    fun setSources(sources: List<Source>) {
        this.sources = sources
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (sources == null) 0 else sources!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView = itemView.text_view_source_name
        var checkbox = itemView.checkbox

        fun bindData(source: Source) {
            textView.text = source.name
            checkbox.isChecked = source.isSelected

            val listener = View.OnClickListener {
                source.isSelected = !source.isSelected
                checkbox.isChecked = source.isSelected
            }

            itemView.setOnClickListener(listener)
            checkbox.setOnClickListener(listener)
        }

    }
}