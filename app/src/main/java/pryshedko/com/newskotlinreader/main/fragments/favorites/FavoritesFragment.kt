package pryshedko.com.newskotlinreader.main.fragments.favorites

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_favorites.view.*
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.base.BaseLifecyclerFragment
import pryshedko.com.newskotlinreader.databinding.FragmentFavoritesBinding
import pryshedko.com.newskotlinreader.entities.Article
import pryshedko.com.newskotlinreader.source.db.LocalDataSource

class FavoritesFragment : BaseLifecyclerFragment<FavoritesViewModel>() {

    override val viewModelClass = FavoritesViewModel::class.java

    private lateinit var rvAdapter: LocalArticlesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var binding = DataBindingUtil.inflate<FragmentFavoritesBinding>(inflater, R.layout.fragment_favorites, container, false)

        binding.viewmodel = viewModel

        rvAdapter = LocalArticlesAdapter(Glide.with(this), { viewModel.deleteArticle(it) }, { openArticleUrl(it) })

        binding.root.rv.adapter = rvAdapter

        observeLiveData()

        return binding.root
    }

    private fun observeLiveData() {
        viewModel.localArticlesLiveData.observe(this, Observer {
            rvAdapter.submitList(it)
        })
    }

    private fun openArticleUrl(article: Article) {

    }

}