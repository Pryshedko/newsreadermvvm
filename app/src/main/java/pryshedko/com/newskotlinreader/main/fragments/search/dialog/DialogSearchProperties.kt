package pryshedko.com.newskotlinreader.main.fragments.search.dialog

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import org.jetbrains.annotations.Nullable
import pryshedko.com.newskotlinreader.R
import pryshedko.com.newskotlinreader.entities.SearchProperties
import pryshedko.com.newskotlinreader.entities.Source
import java.text.SimpleDateFormat
import java.util.*
import android.widget.AdapterView.OnItemSelectedListener


class DialogSearchProperties(var mContext: Context?) : AlertDialog(mContext!!), View.OnClickListener {
    private lateinit var mCallback: (SearchProperties) -> Unit
    private lateinit var mButtonStartSearch: Button
    private lateinit var mButtonCancel: Button
    private lateinit var mButtonDateFrom: Button
    private lateinit var mButtonDateTo: Button
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mSpinner: Spinner

    private val rvAdapter: SourcesAdapter = pryshedko.com.newskotlinreader.main.fragments.search.dialog.SourcesAdapter()
    private var spinnerAdapter: ArrayAdapter<CharSequence>? = null
    private val mCurrentDate: Calendar = Calendar.getInstance()

    var properties: SearchProperties = SearchProperties()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_search_properties)
        initViews()
    }

    private fun initViews() {
        mSpinner = findViewById(R.id.sprinner_sort_by)!!
        mButtonDateFrom = findViewById(R.id.button_date_from)!!
        mButtonDateTo = findViewById(R.id.button_date_to)!!
        mRecyclerView = findViewById(R.id.recycler_view_sources)!!
        mButtonStartSearch = findViewById(R.id.button_start_search)!!
        mButtonCancel = findViewById(R.id.button_cancel)!!

        mButtonDateFrom?.setOnClickListener(this)
        mButtonDateTo?.setOnClickListener(this)
        mButtonStartSearch?.setOnClickListener(this)
        mButtonCancel?.setOnClickListener(this)

        spinnerAdapter = ArrayAdapter
                .createFromResource(mContext, R.array.spinner_data_sort_by, android.R.layout.simple_spinner_dropdown_item)
                .apply { setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) }

        mSpinner?.adapter = spinnerAdapter
        mRecyclerView!!.adapter = rvAdapter

        rvAdapter?.setSources(properties.sources)
        mSpinner?.setSelection(properties.selectedItem)
        mButtonDateFrom?.text = properties.getDateFromStr()
        mButtonDateTo?.text = properties.getDateToStr()


        mSpinner.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                properties.selectedItem = position
            }

            override fun onNothingSelected(parentView: AdapterView<*>) = Unit
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.button_date_from -> showDatePicker(DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                properties.fromDate = GregorianCalendar(year, monthOfYear, dayOfMonth).time
                mButtonDateFrom!!.text = year.toString() + "-" + monthOfYear + "-" + dayOfMonth
            })
            R.id.button_date_to -> showDatePicker(DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                properties.toDate = GregorianCalendar(year, monthOfYear, dayOfMonth).time
                mButtonDateTo!!.text = year.toString() + "-" + monthOfYear + "-" + dayOfMonth
            })
            R.id.button_start_search -> onSearch()
            R.id.button_cancel -> dismiss()
        }
    }

    private fun onSearch() {
        mCallback?.invoke(properties)
        dismiss()
    }

    fun setOnSearchListener(callback: (SearchProperties) -> Unit) {
        mCallback = callback
    }

    private fun showDatePicker(listener: DatePickerDialog.OnDateSetListener) {
        DatePickerDialog(mContext,
                listener,
                mCurrentDate.get(Calendar.YEAR),
                mCurrentDate.get(Calendar.MONTH),
                mCurrentDate.get(Calendar.DAY_OF_MONTH))
                .show()
    }
}