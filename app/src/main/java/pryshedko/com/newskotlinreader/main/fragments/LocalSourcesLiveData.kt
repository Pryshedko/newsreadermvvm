package pryshedko.com.newskotlinreader.main.fragments

import android.arch.lifecycle.MediatorLiveData
import android.util.Log
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pryshedko.com.newskotlinreader.entities.Source
import pryshedko.com.newskotlinreader.source.db.LocalDataSource

class LocalSourcesLiveData : MediatorLiveData<List<Source>>() {
    private var disposable: Disposable? = null

    fun downloadSources(){
        disposable = LocalDataSource.getSources()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it -> value = it
                }, { error -> })
    }

    override fun onInactive() {
        super.onInactive()
        if (disposable?.isDisposed?.not() ?: false) {
            disposable?.dispose()
        }
    }
}